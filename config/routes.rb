Rails.application.routes.draw do
  root 'landing#index'
  devise_for :users

  namespace :api do
    resources :galleries, only: [:index, :show]
    resources :writings, only: [:show]
  end

  namespace :admin do
    resources :galleries
    resources :writings
  end

  get 'galleries/*gallery', to: 'landing#index'
  get '*all', to: 'landing#index'
end
