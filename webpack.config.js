var path = require("path");
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: {
    // The initial source file
    "javascripts/application.js":  "./app/assets/javascripts/application.cjsx"
  },
  output: {
    // The result JavaScript file we are going to require
    // using Sprockets
    path: path.join(__dirname, 'public'),
    filename: "[name]",
    publicPath: "/javascripts/",
    devtoolModuleFilenameTemplate: '[resourcePath]',
    devtoolFallbackModuleFilenameTemplate: '[resourcePath]?[hash]'
  },
  resolve: {
    extensions: ["", ".jsx", ".cjsx", ".coffee", ".js"]
  },
  watchOptions: {
    poll: 1000,
    aggregateTimeout: 1000
  },
  devtool: "source-map",
  module: {
    loaders: [
      // Webpack loaders for processing .coffee and .cjsx files
      { test: /\.cjsx$/, loaders: ["coffee", "cjsx"]},
      { test: /\.coffee$/, loader: "coffee-loader"},
      { test: /\.png$/, loader: "url-loader?limit=100000" }
    ]
  },
  plugins: [
    // Global modules so we don't have to require them manually
    // on each file they are needed
    new webpack.ProvidePlugin({
      // We need to require react/addons so we can use addons
      'React': 'react'
    })
  ]
}
