require "spec_helper"

RSpec.describe ContentConverter do
  describe "to_json" do
    context "given content without leading #" do
      it "returns json empty array" do
        expect(ContentConverter.to_json("abc\nabc")).to eq(JSON.generate([]))
      end
    end

    context "given content with only a header" do
      it "returns array of one object with header" do
        expect(JSON.parse(ContentConverter.to_json("#abc"))[0]["header"]).to eq("abc")
      end

      it "returns array of one object with empty content" do
        expect(JSON.parse(ContentConverter.to_json("#abc"))[0]["paragraphs"]).to eq([])
      end
    end

    context "given content with multiple headers and paragraphs" do
      let(:expected_objects) do
        [{
          "header" => "abc",
          "paragraphs" => ["bbb ccc", "ddd"]
        },
        {
          "header" => "def",
          "paragraphs" => ["eee"]
        }]
      end

      it "returns array of objects with correct info" do
        expect(JSON.parse(ContentConverter.to_json("#abc\nbbb ccc\nddd\n#def\neee"))).to eq(expected_objects)
      end

      context "and extra newlines and spaces" do
        it "returns array of objects with correct info" do
          expect(JSON.parse(ContentConverter.to_json(" \n#abc \n\nbbb ccc\n\nddd \n#def\neee  "))).to eq(expected_objects)
        end
      end
    end
  end

  describe ".to_html" do
    let(:source_json) { JSON.generate([{ header: "abc", paragraphs: ["aaa", "bbb"] }, { header: "def", paragraphs: [] }]) }

    context "given json" do
      it "returns html" do
        expect(ContentConverter.to_html(source_json)).to eq("<h1>abc</h1><p>aaa</p><p>bbb</p><h1>def</h1>")
      end
    end
  end

  describe ".to_text" do
    let(:source_json) { JSON.generate([{ header: "abc", paragraphs: ["aaa", "bbb"] }, { header: "def", paragraphs: [] }]) }

    context "given json" do
      it "returns markdown-like text" do
        expect(ContentConverter.to_text(source_json)).to eq("#abc\naaa\nbbb\n#def\n")
      end
    end
  end
end
