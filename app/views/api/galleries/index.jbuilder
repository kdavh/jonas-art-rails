json.array!(@galleries) do |gallery|
  json.extract! gallery, :permalink, :title, :snippet, :explanation, :category
  json.category_display I18n.t(gallery.category, scope: :gallery_categories)
end
