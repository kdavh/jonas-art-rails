ReactRouter = require('react-router')
Route = ReactRouter.Route
IndexRoute = ReactRouter.IndexRoute
App = require("./components/app")
Gallery = require("./components/gallery")
ContactForm = require("./components/contact_form")
Writing = require("./components/writing")

el = React.createElement

module.exports =
  <Route path="/" component={App}>
    <IndexRoute defaultGalleryPermalink="austin" component={Gallery}/>
    <Route path="galleries/:galleryPermalink" component={Gallery}/>
    <Route path="cv" component={Writing}/>
    <Route path="contact" component={ContactForm}/>
  </Route>
