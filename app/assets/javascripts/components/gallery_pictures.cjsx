# require("./gallery_pictures.scss")
Link = require('react-router').Link

GalleryPictures = React.createClass
  contextTypes:
    location: React.PropTypes.object.isRequired

  render: ->
    self = @

    if @props.loading
      <div className="gallery-pictures">Loading...</div>
    else
      pictures = []
      @props.pictures.forEach (picture) ->
        pictures.push(
          <Link key={picture.title} to={self.context.location.pathname + "?currentPicture=#{picture.title}"} className="gallery-pictures__pic-link">
            <img className="gallery-pictures__pic" src={picture.medium_url} />
          </Link>
        )

      <div className="gallery-pictures">
        <div className="gallery-pictures__scroller gallery-pictures__scroller--prev" onClick={this._scrollPrevPic}></div>
        <div className="gallery-pictures__scroller gallery-pictures__scroller--next" onClick={this._scrollNextPic}></div>
        <div className="gallery-pictures__scroll" ref="galleryScroll">
          {pictures}
        </div>
      </div>

  _scrollNextPic: -> this._scrollIncrementPic(1)
  _scrollPrevPic: -> this._scrollIncrementPic(-1)

  _scrollIncrementPic: (moveAmount) ->
    scrollEl       = this.refs["galleryScroll"]
    middleOfView   = scrollEl.scrollLeft + scrollEl.offsetWidth / 2
    currentElIndex = null

    for i in [0...scrollEl.children.length]
      el = scrollEl.children[i]
      if middleOfView >= el.offsetLeft && middleOfView <= el.offsetLeft + el.offsetWidth
        currentElIndex = i
        break
      else if middleOfView <= el.offsetLeft # middle of view is in the gap between two elements, we went too far
        if moveAmount == 1
          currentElIndex = i - 1
        else
          currentElIndex = i

        break

    nextIndex = currentElIndex + moveAmount
    if nextIndex >= scrollEl.children.length
      nextIndex = 0
    else if nextIndex < 0
      nextIndex = scrollEl.children.length - 1

    nextEl = scrollEl.children[nextIndex]
    scrollEl.scrollLeft = nextEl.offsetLeft + nextEl.offsetWidth / 2 - scrollEl.offsetWidth / 2

module.exports = GalleryPictures
