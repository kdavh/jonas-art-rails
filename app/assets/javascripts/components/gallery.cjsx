# require("./gallery.scss")

GalleryPictures = require("./gallery_pictures.cjsx")
Modal = require("./modal.cjsx")

request = require('superagent')

Gallery = React.createClass
  displayName: "Gallery"
  #
  # contextTypes:
  #   history: React.PropTypes.object.isRequired
  #   location: React.PropTypes.object.isRequired

  propTypes:
    history: React.PropTypes.object.isRequired
    location: React.PropTypes.object.isRequired

  getInitialState: ->
    {
      loading: true,
      permalink: null,
      pictures: [],
      currentPicture: null
    }

  componentDidMount: ->
    @_getNewPictures()

  componentWillReceiveProps: (nextProps) ->
    @_getNewPictures(nextProps)

  render: ->
    modal = null
    if @state.currentPicture
      modal = <Modal picture={@state.currentPicture} onBackgroundClick={@onModalBackgroundClick}/>

    <div className="gallery">
      <GalleryPictures
        pictures={@state.pictures}
        loading={@state.loading} />

      {modal}
    </div>

  onModalBackgroundClick: ->
    baseMatchSet = @props.location.pathname.match(/(.*)(?=\?)/)
    basePath = (baseMatchSet && baseMatchSet[1]) || @props.location.pathname
    @props.history.pushState(null, basePath)

  _getNewPictures: (props) ->
    props ||= @props
    permalink = (props.params && props.params.galleryPermalink) || props.route.defaultGalleryPermalink

    self = @

    if permalink
      request "/api/galleries/#{permalink}", (error, response) ->
        if (!error && response.statusCode == 200)
          pictures = response.body.pictures
          self.setState({ loading: false, currentPicture: null, pictures: pictures })
          # debugger
          match = self.props.location.search.match(/(\?|&)currentPicture\=([^&]*)(&|$)/)
          currentPictureTitle = match && decodeURIComponent(match[2])
          currentPicture = null

          if currentPictureTitle
            currentPicture = pictures.find (picture) ->
              picture.title == currentPictureTitle

          self.setState({ currentPicture: currentPicture })
    else
      self.setState({ loading: false, currentPicture: null, pictures: [] })

module.exports = Gallery
