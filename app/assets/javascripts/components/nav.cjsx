Router = require('react-router')
Link = Router.Link
IndexLink = Router.IndexLink

classnames = require('classnames')

Nav = React.createClass
  displayName: "Nav"

  getInitialState: ->
    { activeMenu: null, isNavActive: false }

  render: ->
    self = @

    galleryCategories = []
    for category, galleries of @props.galleries
      galleryList = []
      galleries.forEach (gallery) ->
        galleryList.push(
          <li key={gallery.permalink} className={classnames(active: self.props.galleryPermalink == gallery.permalink)} onClick={self.handleMenuLinkClick}>
            <Link to={"/galleries/" + gallery.permalink}>{gallery.title}</Link>
          </li>
        )

      galleryCategories.push(
        <div key={category} className={classnames("nav__menu", active: self.state.activeMenu == category)} onClick={this.handleMenuClick} data-menu={category}>
          <h3>{galleries[0].category_display}</h3>
          <ul className="nav__submenu">{galleryList}</ul>
        </div>
      )

    <ul className={classnames("nav", active: self.state.isNavActive)} onClick={this.handleNavClick}>
      <li className={classnames("nav__hero", active: !self.props.galleryPermalink)}>
        <h3><IndexLink to="/">Jonas Hart</IndexLink></h3>
      </li>
      {this._breadcrumbs()}
      <li className="nav__burger"></li>
      <li className="nav__menus">
        {galleryCategories}
        <div className="nav__menu" onClick={this.handleMenuLinkClick}><Link to="/cv">CV</Link></div>
        <div className="nav__menu" onClick={this.handleMenuLinkClick}><Link to="/contact">Contact</Link></div>
      </li>
    </ul>

  handleMenuClick: (event) ->
    if this.state.activeMenu == event.currentTarget.dataset.menu
      this.setState(activeMenu: null)
    else
      this.setState(activeMenu: event.currentTarget.dataset.menu)

    this.setState(isNavActive: true)
    event.stopPropagation()

  handleNavClick: (event) ->
    this.setState(isNavActive: !this.state.isNavActive)

  handleMenuLinkClick: (event) ->
    this.setState(isNavActive: false)
    event.stopPropagation()

  _breadcrumbs: ->
    path = window.location.pathname
    classes = classnames("nav__breadcrumb", "nav__breadcrumb--emphasized" : path == "/cv")

    <li className={classes}>
      {if path != "/" then path.split("/").join(" / ") + " /"}
    </li>

module.exports = Nav
