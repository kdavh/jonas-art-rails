classnames = require('classnames')

ContactForm = React.createClass
  displayName: "ContactForm"

  getInitialState: ->
    { activeMenu: null, isContactFormActive: false }

  render: ->
    <div className="contact-form"><a href="mailto:jonashart@gmail.com">jonashart@gmail.com</a></div>

module.exports = ContactForm
