# require("./app.scss")

Nav = require("./nav")
request = require('superagent')
_ = require("lodash")

App = React.createClass
  getInitialState: ->
    {
      galleries: {}
    }

  componentDidMount: ->
    self = @

    request "/api/galleries", (error, response) =>
      if (!error && response.statusCode == 200)
        self.setState({ galleries: _.groupBy(response.body, "category") })

  render: ->
    <div className="app">
      <Nav galleries={@state.galleries} galleryPermalink={@props.params.galleryPermalink}/>
      {@props.children}
    </div>

module.exports = App
