# require("./modal.scss")

Nav = require("./nav")
request = require('superagent')
classnames = require('classnames')

Modal = React.createClass
  render: ->
    description = []
    (@props.picture.description || "").split("\n").forEach (line, i) ->
      description.push(<div className="modal__description" key={i}>{line}</div>)

    <div className="modal-context active" onClick={@props.onBackgroundClick}>
      <div className="modal">
        <img className="modal__pic" src={@props.picture.medium_url}/>
        <div className="modal__title">{@props.picture.title}</div>
        {description}
      </div>
    </div>

module.exports = Modal
