classnames = require('classnames')
request = require('superagent')

Writing = React.createClass
  displayName: "Writing"

  getDefaultProps: ->
    { type: "cv" }

  componentWillMount: ->
    self = @

    request "/api/writings/#{this.props.type}", (error, response) ->
      if (!error && response.statusCode == 200)
        self.setState({ html: response.body.html_content })

  getInitialState: ->
    { writingHtml: "" }

  render: ->
    <div className= "writing-page" dangerouslySetInnerHTML={this._rawHtml()}/>

  _rawHtml: ->
    { __html: this.state.html }

module.exports = Writing
