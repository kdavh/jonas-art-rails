# require("../stylesheets/application.scss")
routes = require("./routes")
Router = require('react-router').Router
ReactDOM = require("react-dom")
createBrowserHistory = require('history/lib/createBrowserHistory')
history = createBrowserHistory()

FastClick = require('fastclick');
FastClick.attach(document.body);

ReactDOM.render(<Router routes={routes} history={history}/>, document.getElementById("app-content"))
