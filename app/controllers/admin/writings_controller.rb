class Admin::WritingsController < ApplicationController
  layout "admin/application"

  before_action :authorize_admin

  def index
    @writings = Writing.all
  end

  def edit
    @writing = Writing.find(params[:id])
  end

  def update
    @writing = Writing.find(params[:id])
    @writing.update_attributes(writing_params)
  end

  def new
    @writing = Writing.new
  end

  def create
    @writing = Writing.create(writing_params)
  end

  private

  def writing_params
    params.require(:writing).permit(:permalink, :editable_content)
  end
end
