module Api
  class WritingsController < ApplicationController
    layout false
    respond_to :json

    def show
      @writing = Writing.find_by(permalink: params[:id])
    end
  end
end
