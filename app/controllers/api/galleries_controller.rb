module Api
  class GalleriesController < ApplicationController
    layout false
    respond_to :json

    def index
      @galleries = GalleryService.get_all
    end

    def show
      @gallery = GalleryService.get(params[:id])
    end
  end
end
