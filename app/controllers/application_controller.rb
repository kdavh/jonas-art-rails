class ApplicationController < ActionController::Base
  layout :layout_by_resource

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def admin?
    current_user && current_user.email == "jonashart@gmail.com"
  end

  def authorize_admin
    if !admin?
      redirect_to new_user_session_path
    end
  end

  def after_sign_in_path_for(resource)
    admin_galleries_path
  end

  protected

  def layout_by_resource
    if devise_controller?
      "admin/application"
    else
      "application"
    end
  end
end
