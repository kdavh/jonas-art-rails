class GalleryService
  def self.get_all
    Gallery.all.order("category")
  end

  def self.get(permalink)
    gallery = Gallery.find_by!(permalink: permalink)
    gallery.pictures = PictureService.get_for_gallery(gallery)

    gallery
  end
end
