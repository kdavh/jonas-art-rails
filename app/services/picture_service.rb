class PictureService
  include HTTParty

  API_KEY = Rails.application.config.flickr_api_key
  USER_ID = "137779659%40N05"

  default_timeout 1
  base_uri "https://api.flickr.com/services/rest?api_key=#{API_KEY}&user_id=#{USER_ID}"

  class << self
    def get_for_gallery(gallery)
      @@pictures ||= {}
      @@pictures[gallery.id] ||= begin
        photoset_id = gallery.flickr_id
        response = get("&method=flickr.photosets.getPhotos&photoset_id=#{photoset_id}&extras=url_s%2Curl_m%2Curl_o%2Cdescription")

        response["rsp"]["stat"] == "ok" &&
          response["rsp"]["photoset"]["photo"].map { |hash| Picture.from_flickr(hash) }
      end
    end
  end
end
