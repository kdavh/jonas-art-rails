class Picture
  attr_reader :id, :title, :description, :small_url, :small_width, :small_height, :medium_url, :medium_width, :medium_height, :original_url, :original_width, :original_height

  def self.from_flickr(hash)
    new(
      id: hash["id"],
      title: hash["title"],
      description: hash["description"],
      small_url: hash["url_s"],
      small_width: hash["width_s"],
      small_height: hash["height_s"],
      medium_url: hash["url_m"],
      medium_width: hash["width_m"],
      medium_height: hash["height_m"],
      original_url: hash["url_o"],
      original_width: hash["width_o"],
      original_height: hash["height_o"]
    )
  end

  def initialize(attrs = {})
    attrs.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end
end
