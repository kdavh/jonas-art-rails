class ContentConverter
  def self.to_json(text)
    if text.strip[0] != "#"
      return JSON.generate([])
    end

    content_array = []
    paragraphs = text.split(/\n/).map(&:strip).select(&:presence)

    paragraphs.each do |paragraph|
      if paragraph[0] == "#"
        current_section = {}
        content_array << current_section
        current_section[:header] = paragraph[1..-1]
        current_section[:paragraphs] = []
      else
        current_section = content_array[-1]
        current_section[:paragraphs] << paragraph
      end
    end

    JSON.generate(content_array)
  end

  def self.to_text(json)
    JSON.parse(json).reduce("") do |text, hash|
      text += "#" + hash["header"] + "\n"

      if hash["paragraphs"].any?
        text += hash["paragraphs"].join("\n") + "\n"
      end

      text
    end
  end

  def self.to_html(json)
    JSON.parse(json).reduce("") do |text, hash|
      text += "<h1>" + hash["header"] + "</h1>"
      text += hash["paragraphs"].map { |p| "<p>#{p}</p>" }.join("")
    end
  end
end
