class Writing < ActiveRecord::Base
  attr_reader :editable_content

  def editable_content=(text)
    self.content = ContentConverter.to_json(text)
  end

  def editable_content
    ContentConverter.to_text(content)
  end

  def html_content
    ContentConverter.to_html(content)
  end

  private

  def content=(val)
    write_attribute :content, val
  end
end
