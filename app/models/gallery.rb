class Gallery < ActiveRecord::Base
  validates :permalink, :title, :snippet, :flickr_id, presence: true
  validates :permalink, uniqueness: true
  validates :category, inclusion: { in: ["painting", "photo"] }

  def pictures
    @pictures ||= []
  end

  def pictures=(pictures)
    @pictures = pictures
  end
end
