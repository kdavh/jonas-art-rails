var path = require("path");
var webpack = require('webpack');

module.exports = {
  context: process.env.PWD, //__dirname,
  entry: {
    // The initial source file
    "javascripts/application.js":  "./app/assets/javascripts/application.cjsx"
  },
  output: {
    // The result JavaScript file we are going to require
    // using Sprockets
    path: path.join(__dirname, 'public'),
    filename: "[name]",
    publicPath: "/javascripts/",
  },
  resolve: {
    extensions: ["", ".jsx", ".cjsx", ".coffee", ".js"]
  },
  devtool: "source-map",
  module: {
    loaders: [
      // Webpack loaders for processing .coffee and .cjsx files
      { test: /\.cjsx$/, loaders: ["coffee", "cjsx"]},
      { test: /\.coffee$/, loader: "coffee-loader"},
      // { test: /\.scss$/, loaders: ["style", "css", "sass"] },
      // { test: /\.css$/, loaders: ["style", "css"] },
      { test: /\.png$/, loader: "url-loader?limit=100000" }
    ]
  },
  // sassLoader: {
  //   includePaths: require("bourbon").includePaths.concat(require("node-neat").includePaths)
  // },
  plugins: [
    // Global modules so we don't have to require them manually
    // on each file they are needed
    new webpack.ProvidePlugin({
      // We need to require react/addons so we can use addons
      'React': 'react'
    })
  ]
}
