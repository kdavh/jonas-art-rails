class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :permalink, null: false
      t.index :permalink
      t.string :title, null: false
      t.string :snippet, null: false
      t.text :explanation
      t.string :category, null: false, default: "painting"

      t.timestamps null: false
    end
  end
end
