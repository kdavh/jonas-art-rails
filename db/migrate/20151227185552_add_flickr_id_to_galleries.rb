class AddFlickrIdToGalleries < ActiveRecord::Migration
  def change
    add_column :galleries, :flickr_id, :string
  end
end
