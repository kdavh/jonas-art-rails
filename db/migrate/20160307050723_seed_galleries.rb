class SeedGalleries < ActiveRecord::Migration
  def up
    [{"id"=>2,
      "permalink"=>"austin",
      "title"=>"Austin",
      "snippet"=>
       "interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads ",
      "explanation"=>
       "interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads ",
      "flickr_id"=>"72157662855756125",
      category: "painting"},
     {"id"=>3,
      "permalink"=>"kansas",
      "title"=>"Kansas",
      "snippet"=>
       "interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads ",
      "explanation"=>
       "interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads interesting dexcrptioasdfansd sadf dsf asdlkf asdfaslkdakdsfasdads dadflkasdf d fasdlk fads ",
      "flickr_id"=>"72157662415426080",
      category: "painting"}].each do |attrs|

      Gallery.create!(attrs)
    end unless Gallery.any?

    Gallery.create!(
      category: "photo",
      permalink: "yolo",
      title: "Yolo",
      snippet: "asdf asdf laksdf asldkf jasdlf asdf",
      explanation: "asdf alsdfasdfl jasdfalsdfj asdlfjasdl asdflaj sdlfajs dlajsdflkfdaksljd fasdlkf asdf asdf asdf sdf",
      flickr_id: "72157665410994201"
    )
  end

  def down
    Gallery.destroy_all
  end
end
