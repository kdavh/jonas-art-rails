class MakePermalinkUnique < ActiveRecord::Migration
  def up
    change_column :galleries, :permalink, :string, unique: true, length: 200, null: false
  end

  def down
    change_column :galleries, :permalink, :string, null: false
  end
end
