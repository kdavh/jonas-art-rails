class CreateWritings < ActiveRecord::Migration
  def change
    create_table :writings do |t|
      t.string :permalink, null: false, default: ""
      t.text :content, null: false, default: "[]"

      t.timestamps null: false
    end

    Writing.create(
      permalink: "cv",
      editable_content: <<-CONTENT
        #EDUCATION
        MFA  University of Texas at Austin. Studio Art, Painting. 2014

        BFA   Ohio University, Athens, Ohio. Studio Art, Painting. 2010

        #SELECTED EXHIBITIONS

        2016
        Upcoming. Gray Duck Gallery, Austin, TX.

        2015
        "On Plasticity: Poetics of the Built" with Adam Crosson and James Scheuren.
        Lawndale Art Center, Houston, TX.

        2014
        "POOL" (MFA Thesis Exhibition) Visual Arts Center, The University of Texas at Austin

        2013
        "A Catalog" Tiny Park, Austin, TX

        2012
        "MFA Open Studios" The University of Texas at Austin
        2011
        "Spirit of an Appalachian Region" (Juried), OSU Urban Arts Space,
        Columbus, OH
        2010
        "Passing and Bypassing" (Solo). Stuart’s Opera House, Nelsonville, OH
        "The Time Between" (Solo), Ohio Dominican University, Columbus, OH
        "2010
        Photography Show" The Bowen House, Logan, OH "BADASS: The Secret Ingredient" Trisolini Gallery, Athens, OH  "Don’t Waste Your Time Looking at Anything Else" The Union, Athens, OH  "Paintings and Photography" (Solo),The Bowen House, Logan, OH

        2009
        "The Death of Painting Continues ..." Ohio University Gallery, Athens, OH
        "Three Men and a Birthday." Cube4
        Gallery, Athens, OH

        2008
        "This Exit" (Billboard Installation). Ohio State Route 33, Nelsonville, OH
        "Blackoutfest Art Show." Union Arts, Athens, OH

        2003
        "Kansas Scenes" (Juried), Carriage Factory Art Gallery, Newton, KS

        2002
        "Senior Exhibition", Bethel College, North Newton, KS
      CONTENT
    )
  end
end
